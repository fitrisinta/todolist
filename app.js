const express = require('express');
const bodyParser = require('body-parser');
const { compile } = require('ejs');
const date = require(__dirname + '/date.js');
console.log(date);

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

const items = ["Eat salad", "Sleep early", "Watch Netflix"];
const workItems = [];

// Main To Do List
app.get('/', function (req, res) {
    const day = date.getDate();
    res.render('list', { listTitle: day, newListItems: items });
});

app.post('/', function (req, res) {
    const item = req.body.newItem;

    if (req.body.list === "Work") {
        workItems.push(item);
        res.redirect("/work");
    } else {
        items.push(item);
        res.redirect("/");
    }

    console.log(req.body)
    res.redirect('/');
})

// Work To Do List
app.get('/work', function (req, res) {
    const title = "Work"
    res.render('list', { listTitle: title, newListItems: workItems });
});

// About page
app.get('/about', function (req, res) {
    res.render('about');
})

app.listen(3000, function () {
    console.log("Server is running on port 3000");
});
